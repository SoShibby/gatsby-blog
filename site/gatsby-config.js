const wordPressUrl = process.env.GATSBY_WORDPRESS_URL; // URL to your wordpress instance, i.e. http://wordpress.example.com

module.exports = {
  siteMetadata: {
    title: `Mouseless`,
    description: `Mouseless developer`,
    author: `Henrik Nilsson`,
    wordPressUrl,
  },
  __experimentalThemes: [
    {
      resolve: 'gatsby-theme-tabor',
      options: { wordPressUrl },
    },
  ],
};