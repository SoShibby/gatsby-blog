FROM node:12.13.0-buster as builder

WORKDIR /usr/app

# Install dependencies for gatsby-wpgraphql-inline-images
COPY site/plugins/gatsby-wpgraphql-inline-images/package.json site/plugins/gatsby-wpgraphql-inline-images/package.json
RUN (cd site/plugins/gatsby-wpgraphql-inline-images && npm install)

# Install dependencies for gatsby-theme-tabor
COPY packages/gatsby-theme-tabor/package.json packages/gatsby-theme-tabor/package.json
RUN (cd packages/gatsby-theme-tabor && npm install)

# Install dependencies for site and connect site with gatsby-theme-tabor
COPY site/package.json site/package.json
COPY package.json package.json
RUN yarn

# Copy all sources files needed to build the site
COPY site site
COPY packages packages

# Environment variables needed in the build stage
ARG GATSBY_WORDPRESS_URL
RUN echo $GATSBY_WORDPRESS_URL

ARG GATSBY_WORDPRESS_UPLOAD_URL
RUN echo $GATSBY_WORDPRESS_UPLOAD_URL

ARG GATSBY_SITE_URL
RUN echo $GATSBY_SITE_URL

# Build the site
RUN yarn workspace site build

FROM nginx:1.17.4-alpine

COPY --from=builder /usr/app/site/public /usr/share/nginx/html
