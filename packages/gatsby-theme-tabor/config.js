/**
 * I'm thinking we'll probably end up
 * adding this to the .gitignore and have folks configure this on their own?
 *
 * ¯\_(ツ)_/¯
 *
 * @type {{wordPressUrl: string}}
 */
const config = {
  wordPressUrl: process.env.GATSBY_SITE_URL,
  wordPressUploadUrl: process.env.GATSBY_WORDPRESS_UPLOAD_URL
}

module.exports = config
